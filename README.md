# README #

### What is this repository for? ###

* Backend test 
### How to run the code? ###

* 1. Take pull from the git.
* 2. Run the command "npm install" 
* 3. Change the variables and database connection in the .env file 
* 4. Install the migrations (php artisan migrate ) 
* 5. install the seeds. Seeds directly installed from the backendfile provided in the application. (php artisan db:seed)
* 6. Import the file in the postman and configure environment in the postman to test the API's
* 7. Test the code.