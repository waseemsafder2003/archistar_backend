<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PropertyAnalytics extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Create Property
     *
     * @return void
     */
    public function getPropertyAnalytics($id)
    {
        $this->post('/api/$id/analytics/properties', ['state'=>'Victoria'])
             ->seeJson([
                 'created' => true,
             ]);
    }

   
}


