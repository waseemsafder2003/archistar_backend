<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class Analytics_types extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Create Property
     *
     * @return void
     */
    public function getAnalytics()
    {
        $this->post('/api/getAnalytics', ['country' => 'Australia'])
             ->seeJson([
                 'created' => true,
             ]);
    }

}
