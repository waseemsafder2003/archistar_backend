<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PropertyAnalytics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_analytics', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('property_id')->index();
            $table->unsignedBigInteger('analytic_type_id')->index();
            $table->text('value');
            $table->timestamps();
			$table->foreign('property_id')->references('id')->on('properties');
			$table->foreign('analytic_type_id')->references('id')->on('analytic_types');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_analytics');
    }
}
