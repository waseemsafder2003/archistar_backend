<?php

use Illuminate\Database\Seeder;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;

class PropertyTableSeeder extends SpreadsheetSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // specify relative to Laravel project base path
        // specify filename that is automatically dumped from an external process
        $this->file = '/database/seeds/test-data.xlsx';  // note: could alternatively be a csv
        
        $this->worksheetTableMapping = [
            'Properties' => 'properties', 
            'Property_analytics' => 'property_analytics',
            'AnalyticTypes' => 'analytic_types'
        ];
        
        $this->aliases = [
            'Property Id' => 'id', 
            'Suburb' => 'suburb', 
            'State' => 'state',
            'Counrty' => 'country',

            'id' => 'id',
            'name' => 'name',
            'units' => 'units',
            'is_numeric' => 'is_numeric',
            'num_decimal_places' => 'num_decimal_places',

            'propert_id' => 'id',
            'anaytic_type_id' => 'analytic_type_id',
            'value' => 'value',
        ];
        
        parent::run();
    }
}
