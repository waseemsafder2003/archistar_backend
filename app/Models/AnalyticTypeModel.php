<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnalyticTypeModel extends Model
{

    protected $table = 'analytic_types'; 

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'units', 
        'is_numeric',
        'num_decimal_places',
    ];
}
